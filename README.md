# Vue 3 + TypeScript + Vite

## 安装依赖

```js
npm install/yarn install
```

## 运行

```js
npm run dev/yarn dev
```

## 查看

```js
http://localhost:1111/vue3-template/
```
