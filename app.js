// eslint-disable-next-line @typescript-eslint/no-var-requires
const { launch } = require('puppeteer');

function delay(time = 1000) {
  return new Promise(function (resolve) {
    setTimeout(resolve, time);
  });
}

const FLAG = true;

async function giteePageUpdate() {
  const browser = await launch();

  const page = await browser.newPage();

  await page.goto('https://gitee.com/login');

  let email = await page.$x('//*[@id="user_login"]');
  await email[0].type(process.env.email);

  let pwd = await page.$x('//*[@id="user_password"]');
  await pwd[0].type(process.env.password);

  let loginButtons = await page.$x(
    '//*[@class="git-login-form-fields"]/div[4]/input'
  );
  await loginButtons[0].click();

  await delay(2000);

  await page.goto('https://gitee.com/du_zhao_lin/vue3-template/pages');

  await page.on('dialog', async (dialog) => {
    console.log('确认更新');
    dialog.accept();
  });

  let updateButtons = await page.$x('//*[@id="pages-branch"]/div[6]');
  await updateButtons[0].click();

  while (FLAG) {
    await delay(2000);
    try {
      // 获取更新状态标签
      deploying = await page.$x('//*[@id="pages_deploying"]');
      if (deploying.length > 0) {
        console.log('正在更新中');
      } else {
        console.log('更新完毕');
        break;
      }
    } catch (error) {
      break;
    }
  }
  await delay(1000);

  // 8.更新完毕，关闭浏览器
  browser.close();
}
giteePageUpdate();
