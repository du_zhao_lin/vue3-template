import { MockMethod } from 'vite-plugin-mock';
import { resultSuccess } from '../_util';

import data from './_cateList.json';

export default [
  {
    url: '/mock-api/category/list',
    timeout: 0,
    method: 'get',
    response: ({ query }) => {
      let per = query.per * 1 || 1; // 每一页的数量
      let page = query.page || 1; // 页数
      if (page <= 0) {
        page = 1;
      }
      if (per <= 0) {
        per = 1;
      }
      return resultSuccess(data.slice((page - 1) * per, per * page));
    },
  },
] as MockMethod[];
