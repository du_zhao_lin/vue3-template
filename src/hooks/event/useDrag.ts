export function useDrag(): any {
  const left = ref(0);
  const top = ref(0);
  const startLeft = ref(0);
  const startTop = ref(0);

  const isAnimating = ref(false);

  function handleTouchStart(event: TouchEvent) {
    if (isAnimating.value) return;
    const { clientX, clientY } = event.touches[0];
    startLeft.value = clientX - left.value;
    startTop.value = clientY - top.value;
  }

  function handleTouchMove(event: TouchEvent) {
    if (isAnimating.value) return;
    const { clientX, clientY } = event.touches[0];
    // console.log('clientX - startLeft.value', clientX - startLeft.value);
    const boundaryLeft = clientX - startLeft.value;
    const boundaryTop = clientY - startTop.value;
    // if (boundaryLeft <= 0) {
    //   left.value = 0;
    //   return;
    // }
    // if (boundaryLeft >= window.innerWidth - width) {
    //   left.value = window.innerWidth - width;
    //   return;
    // }
    //
    // if (boundaryTop <= 0) {
    //   top.value = 0;
    //   return;
    // }
    // if (boundaryTop >= window.innerHeight - height) {
    //   top.value = window.innerHeight - height;
    //   return;
    // }
    left.value = boundaryLeft;
    top.value = boundaryTop;
  }

  function handleTouchCancel(event: TouchEvent) {
    console.log('event cancel :>> ', event);
    if (isAnimating.value) return;
  }

  function handleTouchEnd(event: TouchEvent) {
    console.log('event end :>> ', event);
    if (isAnimating.value) return;
  }

  return {
    top,
    left,
    isAnimating,
    handleTouchStart,
    handleTouchMove,
    handleTouchCancel,
    handleTouchEnd,
  };
}
