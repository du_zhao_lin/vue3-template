import { onActivated } from 'vue';
import { onBeforeRouteLeave } from 'vue-router';

let globalScrollBox = 'html'; // 全局滚动盒子

export function configKeepScroll(scrollBox: string) {
  console.log('globalScrollBox', scrollBox);
  globalScrollBox = scrollBox;
}

export default function useKeepScroll(scrollBox?: string) {
  let pos = [0, 0];
  let scroller: HTMLElement | null;

  onActivated(() => {
    scroller = document.querySelector(scrollBox || globalScrollBox);
    console.log('scroller: >>>', scroller);
    if (!scroller) {
      console.warn(
        `useKeepScroll: 未找到 ${scrollBox || globalScrollBox} Dom滚动容器`
      );
      return;
    }

    scroller.scrollTop = pos[0];
    scroller.scrollLeft = pos[1];
  });

  onBeforeRouteLeave(() => {
    if (scroller) {
      pos = [scroller.scrollTop, scroller.scrollLeft];
      console.log('pos: >>>', pos);
    }
  });
}
