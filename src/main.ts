import { createApp } from 'vue';
import App from './App.vue';
import { setupStore } from './store';
import { setupPlugins } from './plugins';
import Directive from './plugins/directive';
import router from './router';

import { configKeepScroll } from '@/hooks/useKeepScroll';

import 'virtual:windi.css';
import './styles/main.css';
import '../static/fonts/iconfont.css';

import 'vant/lib/index.css';

configKeepScroll('#app-main-scroller');

const app = createApp(App);

setupPlugins(app);
app.use(router);
app.use(Directive);

setupStore(app);

window.onerror = function (e) {
  console.group('报错信息');
  console.log(['https://stackoverflow.com/search?q=[js]+' + e]);
  console.groupEnd();
};

app.mount('#app');
