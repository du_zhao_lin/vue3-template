import type { Directive, DirectiveBinding } from 'vue';
import { createVNode, render } from 'vue';
import ContextComponent from './ContextMenu.vue';

const CTX_CONTEXTMENU_HANDLER = 'CTX_CONTEXTMENU_HANDLER';

const contextmenuListener = (
  el: HTMLElement,
  event: MouseEvent,
  binding: DirectiveBinding
) => {
  console.log('el', el);
  console.log('event', event);
  console.log('binding', binding);
  event.preventDefault();
  const { stop } = binding.modifiers;
  stop && event.stopPropagation();

  const menus = binding.value;
  if (!menus) return;
  let container: HTMLDivElement | null = null;

  // 移除右键菜单并取消相关的事件监听
  const removeContextmenu = () => {
    if (container) {
      document.body.removeChild(container);
      container = null;
    }
    el.classList.remove('contextmenu-active');
    document.body.removeEventListener('scroll', removeContextmenu);
    window.removeEventListener('resize', removeContextmenu);
  };

  // 创建自定义菜单
  const options = {
    axis: { x: event.x, y: event.y },
    el,
    menus,
    removeContextmenu,
  };

  container = document.createElement('div');
  const vm = createVNode(ContextComponent, options, null);
  render(vm, container);
  document.body.appendChild(container);
};

const ContextmenuDirective: Directive = {
  mounted(el: HTMLElement, binding) {
    console.log('el', el);
    console.log('binding', binding);
    const { capture } = binding.modifiers;
    el[CTX_CONTEXTMENU_HANDLER] = (event: MouseEvent) =>
      contextmenuListener(el, event, binding);
    el.addEventListener('contextmenu', el[CTX_CONTEXTMENU_HANDLER], capture);
  },
  unmounted(el: HTMLElement) {
    console.log('el', el);
  },
};

export default ContextmenuDirective;
