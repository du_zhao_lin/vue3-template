import type { App } from 'vue';

import ContextMenu from './contextmenu';

export default {
  install(app: App): void {
    app.directive('contextmenu', ContextMenu);
  },
};
