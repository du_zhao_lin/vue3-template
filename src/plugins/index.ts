import type { App } from 'vue';

import { i18nPlugins } from './i18n';
import { vantPlugins } from './vant';

export const setupPlugins = (app: App): void => {
  app.use(i18nPlugins).use(vantPlugins);
};
