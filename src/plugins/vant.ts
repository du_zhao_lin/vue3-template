import { App } from 'vue';

import { Button, Card, Tag, Stepper, Popup, Icon, Divider } from 'vant';

const plugins = [Button, Card, Tag, Stepper, Popup, Icon, Divider];

export const vantPlugins = {
  install: function (app: App): void {
    plugins.forEach((item) => {
      app.use(item);
    });
  },
};
