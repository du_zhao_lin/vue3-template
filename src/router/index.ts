import { createRouter, createWebHashHistory, RouteRecordRaw } from 'vue-router';

const routes: Array<RouteRecordRaw> = [
  {
    path: '/',
    redirect: '/xh',
  },
  {
    path: '/hooks',
    name: 'Hook',
    component: () => import('@/views/HookView/index.vue'),
    meta: {
      title: '所有hooks案例',
      transition: 'slide-right',
      keepAlive: true,
    },
  },
  {
    path: '/start',
    name: 'Start',
    component: () => import('@/views/StartPage/index.vue'),
    meta: {
      title: '开始答题',
      transition: 'slide-right',
      keepAlive: true,
    },
  },
  {
    path: '/answer',
    name: 'Answer',
    component: () => import('@/views/Answer/index.vue'),
    meta: {
      title: '答题页面',
      transition: 'slide-right',
      keepAlive: true,
    },
  },
  {
    path: '/score',
    name: 'Score',
    component: () => import('@/views/ScorePage/index.vue'),
    meta: {
      title: '分数展示',
      transition: 'slide-right',
      keepAlive: true,
    },
  },
  {
    path: '/todo',
    name: 'Todo',
    component: () => import('@/views/TodoList/index.vue'),
    meta: {
      title: '待办事项',
      transition: 'slide-right',
      keepAlive: true,
    },
  },
  {
    path: '/clock',
    name: 'Clock',
    component: () => import('@/views/ClockPage/index.vue'),
    meta: {
      title: '时钟',
      transition: 'slide-right',
      keepAlive: true,
    },
  },
  {
    path: '/turntable',
    name: 'Turntable',
    component: () => import('@/views/TurnTable/index.vue'),
    meta: {
      title: '转盘',
      transition: 'slide-right',
    },
  },
  {
    path: '/show',
    name: 'ShowPage',
    component: () => import('@/views/ShowPage/index.vue'),
    meta: {
      title: '3d展示',
      transition: 'slide-left',
      keepAlive: true,
    },
  },
  {
    path: '/xh',
    name: 'Xh',
    component: () => import('@/views/TransitionView/index.vue'),
    meta: {
      title: '雪花',
      transition: 'slide-right',
      keepAlive: true,
    },
  },
  {
    path: '/demos',
    name: 'Demos',
    component: () => import('@/views/Demos/index.vue'),
    meta: {
      title: 'demo-all',
      keepAlive: true,
    },
  },
  {
    path: '/bomb',
    name: 'Bomb',
    component: () => import('@/views/BombPage/index.vue'),
    meta: {
      title: '扫雷',
    },
  },
  {
    path: '/modal',
    name: 'Modal',
    component: () => import('@/views/ModalView/index.vue'),
    meta: {
      title: '弹窗',
      keepAlive: true,
    },
  },
  {
    path: '/drag',
    name: 'DragPage',
    component: () => import('@/views/DragPage/index.vue'),
    meta: {
      title: '拖拽',
      keepAlive: true,
    },
  },
  {
    path: '/contextmenu',
    name: 'ContextMenu',
    component: () => import('@/views/ContextMenu/index.vue'),
    meta: {
      title: '自定义右键',
      keepAlive: true,
    },
  },
  {
    path: '/card',
    name: 'Card',
    component: () => import('@/views/CardView/index.vue'),
    meta: {
      title: '卡片',
      keepAlive: true,
    },
  },
  {
    path: '/video',
    name: 'Video',
    component: () => import('@/views/VideoView/index.vue'),
    meta: {
      title: '拍照',
      keepAlive: true,
    },
  },
  {
    path: '/tree',
    name: 'DataTree',
    component: () => import('@/views/DataTree/index.vue'),
    meta: {
      title: '数据结构案例',
      keepAlive: true,
    },
  },
  {
    path: '/draggable',
    name: 'Draggable',
    component: () => import('@/views/Draggable/index.vue'),
    meta: {
      title: '拖拽',
      keepAlive: true,
    },
  },
  {
    path: '/:pathMatch(.*)',
    name: '404',
    redirect: '/_empty',
  },
];

const router = createRouter({
  history: createWebHashHistory(),
  routes,
  // scrollBehavior(to, _from, savedPosition) {
  //   if (to.hash) {
  //     return {
  //       el: to.hash,
  //       behavior: 'smooth',
  //     };
  //   } else {
  //     if (savedPosition) {
  //       return savedPosition;
  //     } else {
  //       return { top: 0 };
  //     }
  //   }
  // },
});

router.beforeEach((to, _from, next) => {
  if (to.meta.title) {
    document.title = to.meta.title;
  }
  next();
});

export default router;
