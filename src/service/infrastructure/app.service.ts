export class appService {
  constructor() {}

  /**
   * 获取设备cpu的id
   */
  private async getCpuProcessorId(): Promise<string> {
    return new Promise<string>((resolve, reject) => {
      const exec = window['require']('child_process').exec;
      exec('wmic CPU get ProcessorId', (error, stdout, _stderr) => {
        if (error) reject(error);
        else {
          resolve(stdout.split('\n')[1].trim());
        }
      });
    });
  }

  /**
   * 生成唯一设备码
   * @param clientName 客户端名称
   */

  public async fetchOnlyDeviceCode(): Promise<string> {
    const cpuId = await this.getCpuProcessorId();
    const deviceCode = `${cpuId}`;
    return deviceCode;
  }
}
