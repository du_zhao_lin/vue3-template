import createDB from '@/utils/create-indexDB';

import { INotebook, ListQuery } from '/types/noot';

export interface INotebookService {
  create(payload: INotebook): Promise<number>;
  edit(payload: INotebook): Promise<void>;
  get(id: number): Promise<INotebook | undefined>;
  delete(id: number): Promise<void>;
  getList(query: ListQuery): Promise<{ data: INotebook[]; total: number }>;
}
