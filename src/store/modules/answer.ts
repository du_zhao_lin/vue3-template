import { defineStore } from 'pinia';

// {
//   parent_topic_id: 3,
//   select_item_id: 20,
//   select_item_name: '1',
//   is_right: false,
//   grade: 20,
// },

export interface SingeTopicModel {
  readonly parent_topic_id: number;
  readonly select_item_id: number;
  readonly select_item_name: string | number;
  readonly is_right: boolean;
  readonly grade: number;
}

// topic_id: 3,
// topic_name: '学而时习之的下一句',
// selectList:

export interface TopicModel {
  readonly topic_id: number;
  readonly topic_name: string;
  readonly selectList: SingeTopicModel[];
}

export interface StateModel {
  topicList: TopicModel[];
  currentTopicIndex: number;
  answerList: SingeTopicModel[];
}

export enum SortEnum {
  'prev',
  'next',
}

export const answerStore = defineStore('answer', {
  state: (): StateModel => {
    return {
      topicList: [
        {
          topic_id: 1,
          topic_name: '1+1=',
          selectList: [
            {
              parent_topic_id: 1,
              select_item_id: 10,
              select_item_name: 2,
              is_right: true,
              grade: 20,
            },
            {
              parent_topic_id: 1,
              select_item_id: 11,
              select_item_name: 3,
              is_right: false,
              grade: 20,
            },
            {
              parent_topic_id: 1,
              select_item_id: 12,
              select_item_name: 3,
              is_right: false,
              grade: 20,
            },
            {
              parent_topic_id: 1,
              select_item_id: 13,
              select_item_name: 4,
              is_right: false,
              grade: 20,
            },
          ],
        },
        {
          topic_id: 2,
          topic_name: '大闹天宫的是谁',
          selectList: [
            {
              parent_topic_id: 2,
              select_item_id: 15,
              select_item_name: '猪八戒',
              is_right: false,
              grade: 20,
            },
            {
              parent_topic_id: 2,
              select_item_id: 16,
              select_item_name: '孙悟空',
              is_right: true,
              grade: 20,
            },
            {
              parent_topic_id: 3,
              select_item_id: 17,
              select_item_name: '沙和尚',
              is_right: false,
              grade: 20,
            },
            {
              parent_topic_id: 2,
              select_item_id: 18,
              select_item_name: '白骨精',
              is_right: false,
              grade: 20,
            },
          ],
        },
        {
          topic_id: 3,
          topic_name: '学而时习之的下一句',
          selectList: [
            {
              parent_topic_id: 3,
              select_item_id: 20,
              select_item_name: '1',
              is_right: false,
              grade: 20,
            },
            {
              parent_topic_id: 3,
              select_item_id: 21,
              select_item_name: '暂无',
              is_right: false,
              grade: 20,
            },
            {
              parent_topic_id: 3,
              select_item_id: 22,
              select_item_name: '2',
              is_right: false,
              grade: 20,
            },
            {
              parent_topic_id: 3,
              select_item_id: 23,
              select_item_name: '不亦说乎',
              is_right: true,
              grade: 20,
            },
          ],
        },
      ],
      // 当前展示题目的索引
      currentTopicIndex: 0,
      // 收集回答的答案列表
      answerList: [],
    };
  },
  getters: {},
  actions: {
    setTopic(item: SingeTopicModel, fn?: Function) {
      const existItem = this.answerList.find(
        (topic) => topic.parent_topic_id === item.parent_topic_id
      );

      if (!existItem) {
        this.answerList.push(item);
        // 2 < 2 => false
        if (this.currentTopicIndex < this.topicList.length - 1) {
          this.sortTopic(SortEnum.next, 1);
        }
        fn!();
      }
    },
    prevTopic(item: SingeTopicModel, fn?: Function) {
      const existItem = this.answerList.find(
        (topic) => topic.parent_topic_id === item.parent_topic_id
      );
      if (existItem) {
        this.sortTopic(SortEnum.prev, 1);
        fn!();
      }
    },

    sortTopic(sort: SortEnum, num: number) {
      if (sort === SortEnum.prev) {
        this.currentTopicIndex -= num;
      } else {
        this.currentTopicIndex += num;
      }
    },
  },
});
