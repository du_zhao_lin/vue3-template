import { defineStore } from 'pinia';

export const useSettingStore = defineStore('setting', {
  state: () => ({
    showSidebarState: false,
  }),
  getters: {},
  actions: {
    changeSidebarState(data) {
      console.log('data', data);
      this.showSidebarState = data;
    },
  },
});
