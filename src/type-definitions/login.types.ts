/**登录码参数 */
export class loginQRcodeDataType {
  /**
   * 创建时间 */
  CreateTime: string = '';
  /**
   * 过期时间 */
  ExpirationTime: any = '';
  /**
   * 登录编码 */
  LoginCode: string = '';
}

/**登录码状态 */
export class loginQRcodeStateType {
  /**是否登录 */
  IsLogin: boolean = false;
  /**登录编码 */
  LoginCode: string = '';
  /**是否过期 */
  IsExpire: boolean = false;
}
