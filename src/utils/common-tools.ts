const isFunction = (value: any): Boolean =>
  Object.prototype.toString.call(value) === '[object Function]';

/**
 * 用于占用进程
 *
 * @export
 * @param {*} duration 占用时长，单位 ms
 * @returns
 */

const sleep = (duration: number): Promise<any> =>
  new Promise((resolve) => void setTimeout(resolve, duration));

const initPlatform = () => {
  const UA = navigator.userAgent;
  const info = UA.match(/\s{1}DSBRIDGE[\w\.]+$/g);
  if (info && info.length > 0) {
    const infoArray = info[0].split('_');
    window.$appVersion = infoArray[1];
    window.$systemVersion = infoArray[2];
    window.$platform = infoArray[3];
  } else {
    window.$appVersion = '1.0.0';
    window.$systemVersion = undefined;
    window.$platform = 'browser';
  }
};

/**
 * 将字典转成对象数据
 *
 * @export
 * @param {Dictionary<any>} dict
 * @param {string} [keyLabel='value']
 * @param {string} [valueLabel='text']
 * @returns
 */
function transDictToArray(
  dict: Dictionary<any>,
  keyLabel = 'value',
  valueLabel = 'text'
): any {
  const arr = [] as Object[];
  for (const key in dict) {
    // eslint-disable-next-line no-prototype-builtins
    if ((dict as any).hasOwnProperty(key)) {
      arr.push({
        [keyLabel]: key,
        [valueLabel]: dict[key],
      });
    }
  }
  return arr;
}

export { isFunction, sleep, initPlatform, transDictToArray };
