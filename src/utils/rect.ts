import type { Ref } from 'vue';

export interface SiteModel {
  x: number;
  y: number;
}

export interface AllPositionModel {
  allSiteList: SiteModel[];
  drawIndex: Ref<number>;
  dispatchDraw: Function;
}

/**
 * @param xAxisSite
 * @param yAxisSite
 */
export const getAllPosition = (
  xAxisSite: SiteModel,
  yAxisSite: SiteModel
): AllPositionModel => {
  const { x: xStartAxis, y: yStartAxis } = xAxisSite;
  const { x: xEndAxis, y: yEndAxis } = yAxisSite;
  const drawIndex = ref(0);

  console.group('起始位置坐标');
  console.log('xStartAxis', xStartAxis);
  console.log('yStartAxis', yStartAxis);
  console.log('xEndAxis', xEndAxis);
  console.log('yEndAxis', yEndAxis);
  console.groupEnd();

  const minX = Math.min(xStartAxis, xEndAxis);
  const maxX = Math.max(xStartAxis, xEndAxis);
  const minY = Math.min(yStartAxis, yEndAxis);
  const maxY = Math.max(yStartAxis, yEndAxis);

  const allSiteList: SiteModel[] = [];

  for (let x = minX; x <= maxX; x++) {
    for (let y = minY; y <= maxY; y++) {
      if (x % 20 == 0 && y % 20 == 0) {
        allSiteList.push({ x, y });
      }
    }
  }
  console.log('allSiteList', allSiteList);

  function dispatchDraw(value = 1) {
    drawIndex.value += value;
  }

  return {
    allSiteList,
    drawIndex,
    dispatchDraw,
  };
};
