export function copy(text: string): Promise<any> {
  return navigator.clipboard.writeText(text);
}
