export function useTodos(): any {
  const title = ref('');
  const idcard = ref('');
  const group = ref('');
  const date = ref('');

  // interface TodoModel {
  //   title: string;
  //   done: boolean;
  //   id: number;
  // }

  // const todos = computed(
  //   () => JSON.parse(localStorage.getItem('todos') || '[]') || []
  // );

  const todos = computed({
    get: function () {
      return JSON.parse(localStorage.getItem('todos') || '[]') || [];
    },
    set: function (value) {
      localStorage.setItem('todos', JSON.stringify(value));
    },
  });

  function addTodo() {
    if (
      title.value == '' ||
      idcard.value == '' ||
      group.value == '' ||
      date.value == ''
    ) {
      return '';
    }
    todos.value.unshift({
      title: title.value,
      idcard: idcard.value,
      date: date.value,
      group: group.value,
      done: false,
      id: todos.value.length + 1,
    });
    localStorage.setItem('todos', JSON.stringify(todos.value));
    title.value = '';
  }

  function clear() {
    todos.value = todos.value.filter((v) => !v.done);
  }

  const active = computed(() => {
    return todos.value.filter((v) => !v.done).length;
  });

  const all = computed(() => todos.value.length);

  const allDone = computed({
    get: function () {
      return active.value === 0;
    },
    set: function (value) {
      todos.value.forEach((todo) => {
        todo.done = value;
      });
      localStorage.setItem('todos', JSON.stringify(todos.value));
    },
  });

  return {
    title,
    todos,
    addTodo,
    clear,
    all,
    allDone,
    active,
    date,
    idcard,
    group,
  };
}
