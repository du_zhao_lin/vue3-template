export interface INote {
  id: number;
  name: string;
  deadline: Date | undefined;
  isSync: boolean;
  isDone: boolean;
  remark: string;

  [propName: string]: any;
}

export interface INotebook {
  id?: number;
  name: string;
  themeColor: string;
  notes: INote[];

  [propName: string]: any;
}

interface AnyObject {
  [propName: string]: any;
}

export interface ListQuery extends AnyObject {
  page: number;
  count: number;
}
