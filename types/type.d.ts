declare interface Window {
  $appVersion: string;
  $systemVersion: string | undefined;
  $platform: string;
}

interface Dictionary<T> {
  [index: string]: T;
}
