import { UserConfigExport, ConfigEnv } from 'vite';
import vue from '@vitejs/plugin-vue';
import WindiCSS from 'vite-plugin-windicss';
import vueJsx from '@vitejs/plugin-vue-jsx';
import AutoImport from 'unplugin-auto-import/vite';

import Components from 'unplugin-vue-components/vite';

import Icons from 'unplugin-icons/vite';
import IconsResolver from 'unplugin-icons/resolver';

import { promises as fs } from 'fs';

import { visualizer } from 'rollup-plugin-visualizer';

import vueSetupExtend from 'vite-plugin-vue-setup-extend';
import {
  ElementPlusResolver,
  VueUseComponentsResolver,
} from 'unplugin-vue-components/resolvers';

import { resolve } from 'path';

import { viteMockServe } from 'vite-plugin-mock';

import VueI18n from '@intlify/vite-plugin-vue-i18n';

// https://vitejs.dev/config/
export default ({ command }: ConfigEnv): UserConfigExport => {
  return {
    base: '/vue3-template/',
    plugins: [
      vue(),
      vueJsx(),
      vueSetupExtend(),
      WindiCSS(),
      AutoImport({
        include: [/\.[tj]sx?$/, /\.vue\??/],
        dts: './types/auto-imports.d.ts',
        imports: [
          'vue',
          'pinia',
          'vue-router',
          'vue/macros',
          '@vueuse/core',
          'vue-i18n',
        ],
        eslintrc: {
          enabled: false,
          globalsPropValue: true,
          filepath: './.eslintrc-auto-import.json',
        },
      }),
      Components({
        deep: true,
        extensions: ['vue', 'md'],
        dts: 'types/components.d.ts',
        directoryAsNamespace: false,
        globalNamespaces: [],
        directives: true, // vue3 default: true
        include: [/\.vue$/, /\.vue\?vue/],
        exclude: [/[\\/]node_modules[\\/]/, /[\\/]\.git[\\/]/],
        resolvers: [
          ElementPlusResolver(),
          VueUseComponentsResolver(),
          IconsResolver({
            alias: {
              park: 'icon-plus',
            },
            customCollections: ['inline'],
          }),
        ],
      }),
      Icons({
        autoInstall: true,
        customCollections: {
          inline: {
            foo: `<?xml version="1.0" standalone="no"?><!DOCTYPE svg PUBLIC "-//W3C//DTD SVG 1.1//EN" "http://www.w3.org/Graphics/SVG/1.1/DTD/svg11.dtd"><svg t="1656984677981" class="icon" viewBox="0 0 1099 1024" version="1.1" xmlns="http://www.w3.org/2000/svg" p-id="2259" xmlns:xlink="http://www.w3.org/1999/xlink" width="86.9326171875" height="81"><defs><style type="text/css">@font-face { font-family: feedback-iconfont; src: url("//at.alicdn.com/t/font_1031158_u69w8yhxdu.woff2?t=1630033759944") format("woff2"), url("//at.alicdn.com/t/font_1031158_u69w8yhxdu.woff?t=1630033759944") format("woff"), url("//at.alicdn.com/t/font_1031158_u69w8yhxdu.ttf?t=1630033759944") format("truetype"); }
            </style></defs><path d="M283.136 474.624l519.168 0 0 34.816-519.168 0 0-34.816Z" p-id="2260" fill="#2c2c2c"></path><path d="M524.153936 751.643202l2.430137-519.168 34.816 0.162968-2.430137 519.168-34.816-0.162968Z" p-id="2261" fill="#2c2c2c"></path></svg>`,
            async: () => {
              console.log(__dirname);
              return fs.readFile(__dirname + '/src/assets/plus.svg', 'utf-8');
            },
          },
        },
      }),
      viteMockServe({
        ignore: /^\_/,
        mockPath: 'mock',
        localEnabled: command === 'serve',
        prodEnabled: command !== 'serve',
        injectCode: `
          import { setupProdMockServer } from '/mock/_createProductionServer';
          setupProdMockServer();
        `,
      }),
      visualizer(),
      VueI18n({
        runtimeOnly: true,
        compositionOnly: true,
        include: [resolve(__dirname, 'locales/**')],
      }),
    ],
    resolve: {
      alias: [
        {
          find: '@',
          replacement: resolve(__dirname, './src'),
        },
        {
          find: '/types',
          replacement: resolve(__dirname, './types'),
        },
      ],
    },
    build: {
      rollupOptions: {
        output: {
          manualChunks: {},
        },
      },
    },
    server: {
      proxy: {
        '/wxapi': {
          target: 'https://mp.weixin.qq.com/',
          ws: true,
          changeOrigin: true,
        },
      },
    },
  };
};
